import java.util.Scanner;

import com.memetix.mst.language.Language;

import java.net.InetAddress;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;



public class Translator {
	public static void main(String[] args) throws Exception {
		Socket socket = null;//socket connection
		int ss = 0;	//socket
		InetAddress serverIP = null; //address of server
		//end variables
		
		//check CL args
		
		if(args.length != 2){
			System.out.println("Usage: java -jar translatingMessenger.jar {server ip address}{server socket}");
			System.exit(0);
		}
		
		else{
			ss = Integer.parseInt(args[1]);
			serverIP = InetAddress.getByName(args[0]);
		}
		//finish checking args
		//try to connect to server
        try{
        	System.out.println("Connection information: "+serverIP+":"+ss);
            socket = new Socket(serverIP, ss);//opening socket to server specified in the args 
        }
        catch(Exception e){
                System.out.println("The Server has refused the connection.");
                System.exit(0);
        }
        //finish connecting
        //create translator
        CCtranslator t = new CCtranslator("TranslatingMessenger","YY4h0AMA9M4q/3Ty52plCUCU998eE3qHAmwk/7Y8P4c=");
        //get username
        String username = null;
        
        @SuppressWarnings("resource")
		Scanner scan = new Scanner(System.in);
        
        System.out.println("Welcome to the Translating Messenger");
		System.out.print("\nEnter your username: ");
		
		username = scan.nextLine();
		//username retrieved
		//get language 
		System.out.print("Enter your language (type -o for options): ");
		String lang1 = scan.nextLine();
		
		while (!isValidLanguage(lang1)){
			if (lang1.equals("-o")){
				listLanguages();
			}
			System.out.print("\nEnter your language (type -o for options): ");
			lang1 = scan.nextLine();
		}
		
		t.setToLang(Language.valueOf(lang1.toUpperCase()));
		
		t.setFromLang(Language.AUTO_DETECT);
		String yourInfo = "Your language is set to " + t.to.name();
		yourInfo = t.translate(yourInfo);
		System.out.println(yourInfo);
		//language set
		
		//close scanner
		//scan.close();
	    //scanner closed
		//run interface
		clientWriter cw = new clientWriter(socket,username);//for sending messages
		clientReader cr = new clientReader(socket,t);//for reading messages
		ExecutorService threadPool = Executors.newFixedThreadPool(2);
		threadPool.submit(cw);
		threadPool.submit(cr);
		//threadPool.shutdown();
	}




	public static void listLanguages() throws Exception{
		System.out.println("\n---\nLanguages available: \n");
        for(Language lang : Language.values())
            System.out.println(lang.name());
	}
	
	
	
	
	 public static Language validate(String language)
	 {
		 for (Language item : Language.values())
			 if (item.name().equalsIgnoreCase(language))
				 return item;
	        return null;
	}

	 
	 
	 
	public static boolean isValidLanguage(String language)
	{
		return (validate(language) != null);
	}
	    
}