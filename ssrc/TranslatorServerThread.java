import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class TranslatorServerThread extends Thread
{  
	private TranslatorServer server    = null;
	private Socket           socket    = null;
	private int              ID        = -1;
	private BufferedReader  streamIn  = null;
   	private PrintWriter streamOut = null;

   	public TranslatorServerThread(TranslatorServer _server, Socket _socket)
   	{
   		super();
   		server = _server;
   		socket = _socket;
   		ID     = socket.getPort();
   	}
   	public void send(String msg) throws IOException
   	{
   		streamOut.println(msg);
		streamOut.flush();
   	}
   	public int getID()
   	{  
   		return ID;
   	}
   	public void run()
   	{
   		System.out.println("Server Thread " + ID + " running.");
   		while (server.clientCount > 0)
   		{
   			try
   			{  
   				//This is from a previous project
   				String msg = streamIn.readLine();
   				System.out.println(msg);
   				
   				server.handle(ID, msg);
   				//server.handle(ID, CCtranslator.translate(streamIn.readUTF()));
   			}
   			catch(Exception e)
   			{  
   				//System.out.println(ID + " ERROR reading: " + ioe.getMessage());
   				System.out.println(e.getMessage());
   				server.remove(ID);
   				interrupt();
   			}
   		}
   	}
   	public void open() throws IOException
   	{
   		//streamIn = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
   		//streamOut = new PrintWriter(new BufferedOutputStream(socket.getOutputStream()));
   		
   		streamIn = new BufferedReader(( new InputStreamReader(socket.getInputStream())));
		streamOut= new PrintWriter(socket.getOutputStream(),true);
   	}
   	public void close() throws IOException
   	{
   		if (socket != null)    socket.close();
   		if (streamIn != null)  streamIn.close();
   		if (streamOut != null) streamOut.close();
   	}
}
