# translatingMessenger

Messenger that translates

Building:

When you want to build the project simply run ant on the build.XML file, but
Must build server separately with serverBuild.xml

So in order to compile the entire project:
--> ant

--> ant -f serverBuild.xml

Running:

Now you can run the server with:

--> java -jar server.jar [port]

--> java -jar translatingMessenger.jar [server IP] [server port]

--> java -jar translatingMessenger.jar [server IP] [server port]

Now both clients and the server should be running and connected
